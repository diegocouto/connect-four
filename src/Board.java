
public class Board {
	private Character[][] positions = new Character[7][6];
	private Integer score;
	
	public Board() {
		for(int row=0; row < 7; row++) {
			for(int column=0; column < 6; column++) {
				positions[row][column] = new Character('-');
			}
		}
	}
	
	public Character position(Integer row, Integer column) {
		return positions[row][column];
	}
	
	public void setPosition(Integer row, Integer column, Character value) {
		positions[row][column] = value;
	}
	
	// TODO calcular o valor do estado atual do jogo.
	public Integer score() {
		return 10000;
	}
	
	// TODO imprimir estado do tabuleiro.
}